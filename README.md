**This a kata to train on TDD (Test Driven Development)**

We used Node.js/Npm and Chai to create the tests 

To run the tests, you will need to `npm install` first, then `npm run test`


**Rules in French to complete this kata**

L'objectif de ce kata est de réaliser le jeu Puissance 4

L'implémentation proposée consiste en :
- Une grille
- Un analyseur
- Un arbitre

Les responsabilités de ces différents éléments sont :

*Grille*

Une grille de Puissance 4 :
- donne l’état de ses 42 cellules (7 colonnes de 6 rangées)
- accepte jusqu’à 6 jetons dans une colonne
- se vide
- se représente sous forme de texte

*Analyseur*

Un Analyseur de Puissance 4 :
- collabore avec une grille
- sait si un joueur a gagné :
- ligne de 4 jetons de même couleur
- horizontale, verticale, diagonale vers le haut,
- diagonale vers le bas
- sait si la partie est nulle (grille est remplie, aucune ligne gagnante)

*Arbitre*

Un Arbitre de Puissance 4 :
- dit à qui le tour de jouer (jaune ou rouge)
- Jaune commence la partie
- entre le coup du joueur courant
- sait si la partie est en cours, gagnée (et par quel joueur) ou nulle
